from django.urls import path, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()

router.register('invoices', InvoiceListView, base_name='invoices')
#router.register('invoicepositions', InvoicePositionListView, base_name='invoicepositions')
router.register('addresses', AddressListView, base_name='addresses')

urlpatterns = [
    path('', include(router.urls))
]