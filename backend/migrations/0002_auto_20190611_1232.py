# Generated by Django 2.2.2 on 2019-06-11 10:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='address',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.DO_NOTHING, to='backend.Address'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoiceposition',
            name='invoice',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='backend.Invoice'),
            preserve_default=False,
        ),
    ]
