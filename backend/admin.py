from django.contrib import admin

from .models import *

@admin.register(Address)
class AdminAddress(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(Invoice)
class AdminInvoice(admin.ModelAdmin):
    list_display = ('invoice_nr',)


@admin.register(InvoicePosition)
class AdminInvoicePosition(admin.ModelAdmin):
    pass