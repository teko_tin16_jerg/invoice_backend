from django.db import models


class Address(models.Model):
    name=models.CharField(max_length=120)
    street=models.CharField(max_length=120)
    street_nr=models.CharField(max_length=120)
    city=models.CharField(max_length=120)
    zip=models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Invoice(models.Model):
    description=models.TextField()
    invoice_nr=models.IntegerField()
    invoice_date=models.DateField()
    address=models.ForeignKey(Address, on_delete=models.DO_NOTHING)

    def __str__(self):
        return 'Invoice: %s ' % self.invoice_nr


class InvoicePosition(models.Model):
    order_id=models.IntegerField()
    title=models.CharField(max_length=120)
    price=models.FloatField()
    quantity=models.IntegerField()
    invoice=models.ForeignKey(Invoice, related_name='invoice_positions', on_delete=models.CASCADE)

    def __str__(self):
        return self.title
