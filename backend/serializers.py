from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import *


class AddressSerializer(ModelSerializer):

    class Meta:
        model = Address
        fields ='__all__'


class InvoicePositionSerializer(ModelSerializer):
    total_cost = serializers.SerializerMethodField()

    class Meta:
        model = InvoicePosition
        fields = ('order_id', 'price', 'quantity', 'total_cost')

    def get_total_cost(self, obj):
        return obj.price * obj.quantity


class InvoiceSerializer(ModelSerializer):
    invoice_positions = InvoicePositionSerializer(many=True, read_only=True)

    class Meta:
        model = Invoice
        fields =('invoice_nr', 'invoice_date', 'invoice_positions')


